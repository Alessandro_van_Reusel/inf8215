#!/usr/bin/env python3
"""
Avalam agent.
Copyright (C) 2022, Hugo Perronnet - 1885263 / Alessandro Van Reusel - 1889357
Polytechnique Montréal

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.

"""
import sys
from avalam import *


class MyAgent(Agent):

    """My Avalam agent."""

    def minimax_algorithm(self, percepts, player, step, time_left):
        """
        Debut de minmax algo, a ameliorer
        """

        def calculateBoardPoints(board, player):
            """
                Fonction Euristic qui determine la qualité de l'état du Board
                Prend en compte :
                    - Le score
                    - Le nombre de tours de hauteur maximal
                    - Le nombre de tours isolées
            """

            # On donne autant de points que le nouveau score
            score = board.get_score()

            if (board.is_finished()):
                # Si, lorsque la partie est terminée, le score est en faveur du joueur actuel
                # Alors le joueur gagne donc ce coup est automatiquement le meilleur
                if (score * player > 0):
                    return player*1000
                elif (score * player < 0):
                    return -player*1000

            boardTowers = list(board.get_towers())

            for i, j, towerHeight in boardTowers:
                isMovable = board.is_tower_movable(i, j)

                # Si une tour n'est pas "Movable", elle est soit :
                #   - Completement isolée: Aucune tour autour de celle-ci
                #   - Bloquée: Aucune des tours adjacentes ne peut se mettre par dessus
                #   - De hauteur 5
                # Dans ces 3 cas, c'est un point assuré
                if not isMovable:
                    # On attribue un score égal a la hauteur de la tour
                    # Si player = 1:
                    #   Si tour = 5 -> score = 5*1 -> +1 points au score (Player 1 souhaite maximiser le score)
                    #   Si tour = -5 -> score = -5*1 -> -1 points au score (Player 1 souhaite maximiser le score)
                    # Si player = -1:
                    #   Si tour = 5 -> score = 5*-1 -> -1 points au score (Player -1 souhaite minimiser le score)
                    #   Si tour = -5 -> score = 5*1 -> +1 points au score (Player -1 souhaite minimiser le score)
                    score += towerHeight

            return score

        def maxValue(board, currentDepth, maxDepth, alpha, beta):
            if board.is_finished() or maxDepth == currentDepth:
                finalPoints = calculateBoardPoints(board, player)  # Euristic
                return finalPoints, None

            # Cout de la meilleure action
            bestScore = -100000
            # Meilleur action
            bestAction = None

            for action in board.get_actions():
                currentBoard = board.clone()

                nextBoard = currentBoard.play_action(action)
                nextScore, _ = minValue(
                    nextBoard, currentDepth+1, maxDepth, alpha, beta)

                if nextScore > bestScore:
                    bestScore = nextScore
                    bestAction = action
                    alpha = max(alpha, bestScore)
                if bestScore >= beta:
                    return bestScore, bestAction

            return bestScore, bestAction

        def minValue(board, currentDepth, maxDepth, alpha, beta):
            if board.is_finished() or maxDepth == currentDepth:
                finalPoints = calculateBoardPoints(board, player)  # Euristic
                return finalPoints, None

            bestScore = 100000
            bestAction = None

            for action in board.get_actions():
                currentBoard = board.clone()

                nextBoard = currentBoard.play_action(action)
                nextScore, _ = maxValue(
                    nextBoard, currentDepth+1, maxDepth, alpha, beta)

                if nextScore < bestScore:
                    bestScore = nextScore
                    bestAction = action
                    beta = min(beta, bestScore)
                if bestScore <= alpha:
                    return bestScore, bestAction

            return bestScore, bestAction

        print("----------- Start -----------")
        board = dict_to_board(percepts)
        maxDepth = 4

        if (len(list(board.get_actions())) <= 150):
            maxDepth = 5
        if (len(list(board.get_actions())) <= 65):
            maxDepth = 6
        if (len(list(board.get_actions())) <= 50):
            maxDepth = 7
        if (len(list(board.get_actions())) <= 40):
            maxDepth = 8
        if (len(list(board.get_actions())) <= 30):
            maxDepth = 9

        # Si il ne nous reste plus beaucoup de temps, on reduit la prodonfeur de 1
        nbActions = len(list(board.get_actions()))
        if (time_left < 200 and len(list(board.get_actions())) >= 50):
            print("We're running short, reducing depth !")
            maxDepth -= 1
        if (time_left < 50):  # Extreme case
            maxDepth = 3

        # Au debut, on fait toujours les meme actions donc pas la peine de perdre du temps sur les premiere étapes
        if step <= 4:
            maxDepth = 3

        print(len(list(board.get_actions())))

        print("player:", player)
        print("step:", step)
        print("time left:", time_left if time_left else '+inf')
        print("Depth : ", maxDepth)
        print("potential iterations: ", pow(nbActions, maxDepth))
        score, action = maxValue(
            board.clone(), 1, maxDepth, -sys.maxsize - 1, sys.maxsize) if player == 1 else minValue(board.clone(), 1, maxDepth, -sys.maxsize - 1, sys.maxsize)

        print("action : ", action)
        print("----------- Finish -----------")
        return action

    def play(self, percepts, player, step, time_left):
        """
        This function is used to play a move according
        to the percepts, player and time left provided as input.
        It must return an action representing the move the player
        will perform.
        :param percepts: dictionary representing the current board
            in a form that can be fed to `dict_to_board()` in avalam.py.
        :param player: the player to control in this step (-1 or 1)
        :param step: the current step number, starting from 1
        :param time_left: a float giving the number of seconds left from the time
            credit. If the game is not time-limited, time_left is None.
        :return: an action
            eg; (1, 4, 1 , 3) to move tower on cell (1,4) to cell (1,3)
        """

        action = self.minimax_algorithm(
            percepts, player, step, time_left)

        return action


if __name__ == "__main__":
    agent_main(MyAgent())
