#!/usr/bin/env python3
"""
Avalam agent.
Copyright (C) 2022, Hugo Perronnet - 1885263 / Alessandro Van Reusel - 1889357
Polytechnique Montréal

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.

"""
import sys
from avalam import *


class MyAgent(Agent):

    """My Avalam agent."""

    minCount = 0
    maxCount = 0

    def minimax_algorithm(self, percepts, player, step, time_left):
        """
        Debut de minmax algo, a ameliorer
        """
        self.minCount = 0
        self.maxCount = 0

        def maxValue(board, currentDepth, maxDepth, alpha, beta):
            self.maxCount += 1  # For debuging only

            if board.is_finished() or maxDepth == currentDepth:
                return board.get_score(), None

            # Cout de la meilleure action
            bestScore = -100000
            # Meilleur action
            bestAction = None

            for action in board.get_actions():
                currentBoard = board.clone()

                nextBoard = currentBoard.play_action(action)
                nextScore, _ = minValue(
                    nextBoard, currentDepth+1, maxDepth, alpha, beta)

                if nextScore > bestScore:
                    bestScore = nextScore
                    bestAction = action
                    alpha = max(alpha, bestScore)
                if bestScore >= beta:
                    print("pruning")
                    return bestScore, bestAction

            return bestScore, bestAction

        def minValue(board, currentDepth, maxDepth, alpha, beta):
            self.minCount += 1  # For debuging only

            if board.is_finished() or maxDepth == currentDepth:
                return board.get_score(), None

            bestScore = 100000
            bestAction = None

            for action in board.get_actions():
                currentBoard = board.clone()

                nextBoard = currentBoard.play_action(action)
                nextScore, _ = maxValue(
                    nextBoard, currentDepth+1, maxDepth, alpha, beta)

                if nextScore < bestScore:
                    bestScore = nextScore
                    bestAction = action
                    beta = min(beta, bestScore)
                if bestScore <= alpha:
                    print("pruning")
                    return bestScore, bestAction

            return bestScore, bestAction

        board = dict_to_board(percepts)
        maxDepth = 3

        if (len(list(board.get_actions())) <= 40):
            maxDepth = 4

        print(len(list(board.get_actions())))

        # Si on est player -1, on veut "minimiser" notre score (en negatif le plus posible)
        score, action = maxValue(
            board.clone(), 1, maxDepth, -sys.maxsize - 1, sys.maxsize) if player == 1 else minValue(board.clone(), 1, maxDepth, -sys.maxsize - 1, sys.maxsize)

        print("action : ", action)
        print("minCount : ", self.minCount)
        print("maxCount : ", self.maxCount)
        return action

    def play(self, percepts, player, step, time_left):
        """
        This function is used to play a move according
        to the percepts, player and time left provided as input.
        It must return an action representing the move the player
        will perform.
        :param percepts: dictionary representing the current board
            in a form that can be fed to `dict_to_board()` in avalam.py.
        :param player: the player to control in this step (-1 or 1)
        :param step: the current step number, starting from 1
        :param time_left: a float giving the number of seconds left from the time
            credit. If the game is not time-limited, time_left is None.
        :return: an action
            eg; (1, 4, 1 , 3) to move tower on cell (1,4) to cell (1,3)
        """
        print("percept:", percepts)
        print("player:", player)
        print("step:", step)
        print("time left:", time_left if time_left else '+inf')

        # TODO: implement your agent and return an action for the current step.

        # Changer la maxDepth en fonction du step
        # Changer la maxDepth en fonction du temp restant (si il reste peu de temps, on diminue le maxDepth)

        action = self.minimax_algorithm(
            percepts, player, step, time_left)

        return action


if __name__ == "__main__":
    agent_main(MyAgent())
