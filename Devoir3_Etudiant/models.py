import nn


class PerceptronModel(object):
    def __init__(self, dimensions):
        """
        Initialize a new Perceptron instance.

        A perceptron classifies data points as either belonging to a particular
        class (+1) or not (-1). `dimensions` is the dimensionality of the data.
        For example, dimensions=2 would mean that the perceptron must classify
        2D points.
        """
        self.w = nn.Parameter(1, dimensions)

    def get_weights(self):
        """
        Return a Parameter instance with the current weights of the perceptron.
        """
        return self.w

    def run(self, x):
        """
        Calculates the score assigned by the perceptron to a data point x.

        Inputs:
            x: a node with shape (1 x dimensions)
        Returns: a node containing a single number (the score)
        """
        "*** TODO: COMPLETE HERE FOR QUESTION 1 ***"
        return nn.DotProduct(x, self.w)

    def get_prediction(self, x):
        """
        Calculates the predicted class for a single data point `x`.

        Returns: 1 or -1
        """
        "*** TODO: COMPLETE HERE FOR QUESTION 1 ***"
        return 1 if nn.as_scalar(self.run(x)) >= 0 else -1

    def train(self, dataset):
        """
        Train the perceptron until convergence.
        """
        "*** TODO: COMPLETE HERE FOR QUESTION 1 ***"
        hasError = True
        while hasError:
            hasError = False
            for x, y in dataset.iterate_once(1):
                if nn.as_scalar(y) != self.get_prediction(x):
                    self.w.update(x, nn.as_scalar(y))
                    hasError = True


class RegressionModel(object):
    """
    A neural network model for approximating a function that maps from real
    numbers to real numbers. The network should be sufficiently large to be able
    to approximate sin(x) on the interval [-2pi, 2pi] to reasonable precision.
    """

    def __init__(self):
        # Initialize your model parameters here
        "*** TODO: COMPLETE HERE FOR QUESTION 2 ***"
        # 1 param d'entrée
        # 10 neurones sur couche 1
        # 10 neurones sur couche 2
        # 1 param de sortie
        self.neurones = [1, 10, 10, 1]
        self.couches = 3

        self.weights = []
        self.bias = []
        self.mini_batch = 25  # Taille du dataset = 200 --> 25 est diviseur
        self.taux_apprentissage = -0.03

        for i in range(1, self.couches+1):
            self.weights.append(nn.Parameter(
                self.neurones[i-1], self.neurones[i]))

            self.bias.append(nn.Parameter(1, self.neurones[i]))

    def run(self, x):
        """
        Runs the model for a batch of examples.

        Inputs:
            x: a node with shape (batch_size x 1)
        Returns:
            A node with shape (batch_size x 1) containing predicted y-values
        """
        "*** TODO: COMPLETE HERE FOR QUESTION 2 ***"
        newData = x

        for i in range(0, self.couches):
            newData = nn.Linear(
                newData if i == 0 else nn.ReLU(newData), self.weights[i])
            newData = nn.AddBias(newData, self.bias[i])

        return newData

    def get_loss(self, x, y):
        """
        Computes the loss for a batch of examples.

        Inputs:
            x: a node with shape (batch_size x 1)
            y: a node with shape (batch_size x 1), containing the true y-values
                to be used for training
        Returns: a loss node
        """
        "*** TODO: COMPLETE HERE FOR QUESTION 2 ***"
        return nn.SquareLoss(self.run(x), y)

    def train(self, dataset):
        """
        Trains the model.
        """
        "*** TODO: COMPLETE HERE FOR QUESTION 2 ***"
        hasError = True
        current_layer = 0
        while hasError:
            hasError = False
            for x, y in dataset.iterate_once(self.mini_batch):
                loss = self.get_loss(x, y)

                if nn.as_scalar(loss) >= 0.02:
                    hasError = True
                    # Update weight et bias pour chaque couche en fonction du gradient
                    grad_w, grad_b = nn.gradients(
                        loss, [self.weights[current_layer], self.bias[current_layer]])
                    self.weights[current_layer].update(
                        grad_w, self.taux_apprentissage)
                    self.bias[current_layer].update(
                        grad_b, self.taux_apprentissage)

            current_layer = (current_layer + 1) % self.couches


class DigitClassificationModel(object):
    """
    A model for handwritten digit classification using the MNIST dataset.

    Each handwritten digit is a 28x28 pixel grayscale image, which is flattened
    into a 784-dimensional vector for the purposes of this model. Each entry in
    the vector is a floating point number between 0 and 1.

    The goal is to sort each digit into one of 10 classes (number 0 through 9).

    (See RegressionModel for more information about the APIs of different
    methods here. We recommend that you implement the RegressionModel before
    working on this part of the project.)
    """

    def __init__(self):
        # Initialize your model parameters here
        "*** TODO: COMPLETE HERE FOR QUESTION 3 ***"
        # Pour trouver les parametres de couches, neurones, mini_batch et taux d'apprentissage,
        # nous avons créé un script qui lance l'alogirthme pour des parametres randomisé
        # Ces paramtres nous donnent les meilleurs résultats

        # 784 params d'entrée
        # 20 neurones sur couche 1
        # 20 neurones sur couche 2
        # 10 params de sortie
        self.neurones = [784, 231, 10]
        self.couches = len(self.neurones)-1

        self.weights = []
        self.bias = []
        self.mini_batch = 480
        self.taux_apprentissage = -0.6847248218482526

        for i in range(1, self.couches+1):
            self.weights.append(nn.Parameter(
                self.neurones[i-1], self.neurones[i]))

            self.bias.append(nn.Parameter(1, self.neurones[i]))

    def run(self, x):
        """
        Runs the model for a batch of examples.

        Your model should predict a node with shape (batch_size x 10),
        containing scores. Higher scores correspond to greater probability of
        the image belonging to a particular class.

        Inputs:
            x: a node with shape (batch_size x 784)
        Output:
            A node with shape (batch_size x 10) containing predicted scores
                (also called logits)
        """
        "*** TODO: COMPLETE HERE FOR QUESTION 3 ***"
        newData = x

        for i in range(0, self.couches):
            newData = nn.Linear(
                newData if i == 0 else nn.ReLU(newData), self.weights[i])
            newData = nn.AddBias(newData, self.bias[i])

        return newData

    def get_loss(self, x, y):
        """
        Computes the loss for a batch of examples.

        The correct labels `y` are represented as a node with shape
        (batch_size x 10). Each row is a one-hot vector encoding the correct
        digit class (0-9).

        Inputs:
            x: a node with shape (batch_size x 784)
            y: a node with shape (batch_size x 10)
        Returns: a loss node
        """
        "*** TODO: COMPLETE HERE FOR QUESTION 3 ***"
        return nn.SoftmaxLoss(self.run(x), y)

    def train(self, dataset):
        """
        Trains the model.
        """
        "*** TODO: COMPLETE HERE FOR QUESTION 3 ***"
        while dataset.get_validation_accuracy() <= 0.971:
            for x, y in dataset.iterate_once(self.mini_batch):
                loss = self.get_loss(x, y)
                # Update weight et bias pour chaque couche en fonction du gradient
                grads = nn.gradients(loss, self.weights + self.bias)

                for i in range(len(grads)):
                    # nbCouches premiers gradients = gradients de w, les autres = gradients de bias
                    if (i < self.couches):
                        self.weights[i].update(
                            grads[i], self.taux_apprentissage)
                    else:
                        self.bias[i % self.couches].update(
                            grads[i], self.taux_apprentissage)
