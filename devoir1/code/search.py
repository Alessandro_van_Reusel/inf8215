# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).

# Hugo Perronnet - 1885263
# Alessandro Van Reusel - 1889357


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return [s, s, w, s, w, w, s, w]


def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """
    # Creation of the stack
    fringe = util.Stack()
    # Push a tuple with a position and a path array (["South","West" ...])
    fringe.push((problem.getStartState(), []))
    # Creation of the visited state in a set
    visited = []
    while not fringe.isEmpty():
        # Pop the last element pushed in the stack
        state, path = fringe.pop()
        if problem.isGoalState(state):
            return path
        # If the state was not yet visited
        if state not in visited:
            # Get all successors
            successors = problem.getSuccessors(state)
            for position, direction, cost in successors:
                # Push the successor's position and the direction + the path of the current state
                fringe.push((position, path + [direction]))
            # Add the current state in visited
            visited.append(state)


def breadthFirstSearch(problem):
    # Creation of the queue
    fringe = util.Queue()
    # Push a tuple with a position and a path array (["South","West" ...])
    fringe.push((problem.getStartState(), []))
    # Creation of the visited state in a set
    visited = []

    while not fringe.isEmpty():
        # Pop the first state pushed in the queue
        state, path = fringe.pop()
        if problem.isGoalState(state):
            return path
        # If the state was not yet visited
        if state not in visited:
            # Get all successors
            successors = problem.getSuccessors(state)
            for position, direction, cost in successors:
                fringe.push((position, path + [direction]))
            # Add the current state in visited
            visited.append(state)


def uniformCostSearch(problem):
    # Creation of the priority queue
    fringe = util.PriorityQueue()
    # Push a tuple with a position, a path array (["South","West" ...]) and the cost
    fringe.push((problem.getStartState(), [], 0), 0)
    # Creation of the visited state in a set
    visited = []
    while not fringe.isEmpty():
        # Pop the least costly state of the priority queue
        state, path, c = fringe.pop()
        if problem.isGoalState(state):
            return path
        # If the state was not yet visited
        if state not in visited:
            # Get all successors
            successors = problem.getSuccessors(state)
            for position, direction, cost in successors:
                # c + cost : cost to go to the current state + cost to go to the successor state
                fringe.push((position, path + [direction], c + cost), c + cost)
            # Add the current state in visited
            visited.append(state)


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """

    return 0


def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    '''
        INSÉREZ VOTRE SOLUTION À LA QUESTION 4 ICI
    '''
    fringe = util.PriorityQueue()
    fringe.push((problem.getStartState(), [], 0), 0)
    visited = []
    while not fringe.isEmpty():
        state, path, currentCost = fringe.pop()
        if problem.isGoalState(state):
            return path
        if state not in visited:
            successors = problem.getSuccessors(state)
            for position, direction, succCost in successors:
                newEstimation = currentCost + succCost + \
                    heuristic(position, problem)
                fringe.push((position, path + [direction],
                             currentCost + succCost), newEstimation)
            visited.append(state)


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
